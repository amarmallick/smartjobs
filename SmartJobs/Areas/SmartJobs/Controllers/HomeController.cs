﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartJobs.Areas.SmartJobs.Controllers
{
    public class HomeController : Controller
    {
        // GET: SmartJobs/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}