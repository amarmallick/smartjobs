﻿using System.Web.Mvc;

namespace SmartJobs.Areas.SmartJobs
{
    public class SmartJobsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SmartJobs";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SmartJobs_default",
                "SmartJobs/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}