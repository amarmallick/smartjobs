﻿using System.Web.Mvc;

namespace SmartJobs.Areas.SmartLearning
{
    public class SmartLearningAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SmartLearning";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SmartLearning_default",
                "SmartLearning/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}