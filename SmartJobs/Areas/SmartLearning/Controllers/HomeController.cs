﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartJobs.Areas.SmartLearning.Controllers
{
    public class HomeController : Controller
    {
        // GET: SmartLearning/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}