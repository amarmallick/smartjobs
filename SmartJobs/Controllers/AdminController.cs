﻿using System.Web.Mvc;

namespace SmartJobs.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Central(string uk)
        {
            if (string.IsNullOrEmpty(uk))
                return RedirectToAction("login", "account");
            return View("Central");
            //if (!string.IsNullOrEmpty(uk))
            //{
            //    HttpClientHelper<LoggedInUser> helper = new HttpClientHelper<LoggedInUser>();
            //    LoggedInUser loggedInUser = helper.GetSingle(string.Format("users/get-user?uk={0}", uk));

            //    if (loggedInUser == null)
            //        return RedirectToAction("login", new { msg = "loggedout" });

            //    Session["LOGIN_USER"] = loggedInUser;

            //    switch (loggedInUser.Role)
            //    {
            //        case (int)URole.Admin:
            //            return RedirectToAction("index", "admin");
            //        case (int)URole.Tutor:
            //            return RedirectToAction("index", "tutor");
            //        case (int)URole.Student:
            //            return RedirectToAction("index", "student");
            //    }
            //    return RedirectToAction("login", new { msg = "loggedout" });
            //}
            //return RedirectToAction("login", new { msg = "loggedout" });
        }
    }
}