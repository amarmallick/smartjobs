﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartJobs.Controllers
{
    public class AccountController : Controller
    {
        [ActionName("login")]
        public ActionResult Login()
        {
            return View("Login");
        }
    }
}