﻿(function ($) {
    "use strict";
    var common = {
    }

    $(document).ready(function () {

        // Ping Server
        //var intervalid = window.setInterval(function Redirect() {
        //    checkServer();
        //}, 1000 * 60); // Runs every 60 seconds (60000 ms)

        // Global AJAX request settings
        $(document).ajaxStart(function () {
            //$("body").loading({ theme: 'dark' });
        }).ajaxStop(function () {
            //$('body').loading('stop');
        }).ajaxComplete(function (a, b, c) {
            //console.log(b);
            if (b.statusText != "OK" && b.statusText != "invalid" && b.statusText !="not_approved") {
                if (b.statusText == "error")
                    toastr.error('There was some error connecting to server.');
                if (b.statusText == "timeout")
                    toastr.error('Timeout occurred. Please try again.');
                //else
                //    toastr.error(b.statusText);
            }
        });
    });
}(jQuery));

//convert to json
function toJSONString(form) {
    var obj = {};
    var elements = form.querySelectorAll("input, select, textarea");
    for (var i = 0; i < elements.length; ++i) {
        var element = elements[i];
        var name = element.name;
        var value = element.value;

        if (name) {
            obj[name] = value;
        }
    }

    return JSON.stringify(obj);
}

function showErrors(error) {
    //if (JSON.parse(error.responseText).Errors.length > 0) {
    //    var htm = "<ul style='margin-left:-20px;'>";
    //    for (var i = 0; i < JSON.parse(error.responseText).Errors.length; i++) {
    //        var err = JSON.parse(error.responseText).Errors[i];
    //        if (err != null)
    //            var htm = htm + "<li>" + err + "</li>";
    //    }
    //    var htm = htm + "</ul>";
    //    toastr.error(htm);
    //}
    //else 
    //toastr.error(JSON.parse(error.responseText).Message);
    console.log(error);
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function timedRedirect(url, delay) {
    setTimeout(function () {
        window.location.href = url;
    }, delay);
}

// Hit Server
function checkServer() {
    $.get("/Home/ServerPing", function (data) {
        //console.log(data);
    });
}

function setActiveCss() {
    var current = location.pathname;
   
    $('#side-main-menu li a').each(function () {
        var $this = $(this);
        var selected_li = $this.attr('href').substring($this.attr('href').lastIndexOf('/') + 1, $this.attr('href').length);
        // if the current path is like this link, make it active
        if ($this.attr('href').replace('/', '') == current.replace('/','')) {
            $this.parent("li").addClass("active");
        }
    })
}