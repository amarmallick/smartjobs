﻿(function ($) {
    "use strict";

    var resources = {
        loadLinks: function () {
            loadTable();
        }
    }

    // init
    $(document).ready(function () {
        // load links list
        resources.loadLinks();
    });
}(jQuery));

function loadTable() {
    $('#list-links').dataTable({
        destroy: true,
        searching: false,
        "paging": false,
        "info": false,
        "ajax": {
            "url": remoteURL + "FreeResources/get-free-resources?rtId=1",
            "dataSrc": ""
        },
        "columns": [
            {
                "data": "LinkTitle", "sortable": false
            },
            {
                "data": "LinkUrl", "sortable": false
            },
            {
                "data": "DocType", "sortable": false
            },
            {
                "data": "IsDoc", "sortable": false, "render": function (url, type, full) {
                    if (full.IsDoc) {
                        return '<div class="text-right"><button class="btn btn-xs btn-info" onclick="download(\'' + full.DocPath + '\')">Download</button></div>'
                    }
                    else {
                        return '<div class="text-right"><a class="text-accent-dark" href="' + full.LinkUrl + '" target="_blank">View HTML</a></div>'
                    };
                }
            }
        ]
    });
}

function download(linkUrl) {
    window.open('http://localhost:54444/content/images/connect-smart-192x192.png');
}