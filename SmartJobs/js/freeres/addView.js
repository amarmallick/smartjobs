﻿(function ($) {
    "use strict";
    $.fn.serializeFormJSON = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    var freeResource = {
        // add resource
        add: function (form_data) {
            var data = new FormData();

            var files = $("#fileUpload").get(0).files;

            // Add the uploaded image content to the form data collection
            if (files.length > 0) {
                data.append("UploadedImage", files[0]);
            }
            console.log(form_data);
            // Make Ajax request with the contentType = false, and procesDate = false
            var ajaxRequest = $.ajax({
                type: "POST",
                url: remoteURL + "FreeResources/UploadFile?linkTitle=" + form_data.LinkTitle +
                "&linkUrl=" + form_data.LinkUrl + "&resourceTitleId=1",
                contentType: false,
                processData: false,
                data: data
            });

            ajaxRequest.done(function (xhr, textStatus) {
                // Do other operation
                $('#linksModal').modal('toggle');
            });
        },

        // add resource title
        addTitle: function (form_data) {
            //var json = JSON.stringify(form_data);
            console.log(form_data);

            var jqxhr = $.post(remoteURL + "FreeResources/add-update-resource-title", form_data)
                .done(function (data) {
                    // Load Resources
                    freeResource.loadResources();
                }).fail(function (xhr) {
                    console.log(xhr);
                    showErrors(xhr);
                })
        },

        loadResources: function () {
            $('#list-resources').dataTable({
                destroy: true,
                searching: false,
                "paging": false,
                //"info": false,
                "ajax": {
                    "url": remoteURL + "FreeResources/get-resource-title",
                    //"beforeSend": function (xhr) {
                    //    xhr.setRequestHeader("Authorization", "Basic " + Cookies.get('auth'));
                    //},
                    "dataSrc": ""
                },
                "columns": [
                    {
                        "data": "Title"
                    },
                    //{
                    //    "data": "AddedOn",
                    //    "render": function (data) {
                    //        return moment(data).format('DD/MM/YYYY');
                    //    }
                    //},
                    //{
                    //    "data": "AddedBy"
                    //},
                    {
                        "data": null, "sortable": false, "render": function (url, type, full) {
                            return '<div class="pull-right"><a href="" class="text-accent-dark" data-toggle="modal" data-target="#linksModal">Add Links</a>&nbsp;|&nbsp;' +
                                '<a class="text-accent-dark" href="/SmartLearning/FreeResources/resource-links?id=' + full.ID + '">View Links</a></div>';
                        }
                    }
                ],

            });
        },
    }

    $(document).ready(function () {
        // Add resource title
        $("#free-resource-title").on("submit", function (event) {
            event.preventDefault();
            var form_data = $(this).serializeFormJSON();

            freeResource.addTitle(form_data);
        });
        // Add resource
        $("#free-resource-form").on("submit", function (event) {
            event.preventDefault();
            var form_data = $(this).serializeFormJSON();

            freeResource.add(form_data);
        });

        // init
        // load resources list
        freeResource.loadResources();
    });
}(jQuery));




