﻿(function ($) {
    "use strict";
    var registration = {
        // add case
        addUser: function (form_data) {
            var json = JSON.stringify(form_data);
            console.log(form_data);

            var jqxhr = $.post(baseURL + "account/register", form_data )
                .done(function (data) {
                    toastr.options = {
                        "positionClass": "toast-top-center",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                    toastr.success('Congratulations. Your account has been created');
                    $("#registerModal").modal("hide");
                }).fail(function (xhr) {
                    console.log(xhr);
                    showErrors(xhr);
                })
        },
    }

    $(document).ready(function () {
        $("#register").on("submit", function (event) {
            event.preventDefault();
            var form_data = $(this).serialize();
            //console.log(form_data);
            registration.addUser(form_data);
        });
    });
}(jQuery));




