﻿(function ($) {
    "use strict";
    $.fn.serializeFormJSON = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    var notif = {
        // add notification
        add: function (form_data) {
            var jqxhr = $.post(remoteURL + "notifications/add", form_data)
                .done(function (data) {
                    // redirect
                    window.location.href = '/smartLearning/notifications/index';
                }).fail(function (xhr) {
                    console.log(xhr);
                    showErrors(xhr);
                })
        }
    }

    $(document).ready(function () {
        // Add notification
        $("#notif-form").on("submit", function (event) {
            event.preventDefault();

            var form_data = $(this).serializeFormJSON();
            notif.add(form_data);
        });
    });
}(jQuery));




