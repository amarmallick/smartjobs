namespace SmartJobs.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class user_dev : DbMigration
    {
        public override void Up()
        {
            
            CreateTable(
                "dbo.UserDevice",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DeviceID = c.Short(nullable: false),
                        UserID = c.Int(nullable: false),
                        DeviceTypeId = c.Short(nullable: false),
                        DeviceOsVersion = c.String(),
                        SmartPhoneID = c.String(),
                        AppVersionOnDevice = c.String(nullable: false, maxLength: 100),
                        PushNotificationID = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                        TimeZone = c.String(),
                        LastLogin = c.DateTime(nullable: false),
                        NotifyTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
           
            
            
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRole", "Users_ID", "dbo.Users");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Roles");
            DropIndex("dbo.UserRole", new[] { "Users_ID" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropTable("dbo.Users");
            DropTable("dbo.UserRole");
            DropTable("dbo.UserDevice");
            DropTable("dbo.Roles");
        }
    }
}
