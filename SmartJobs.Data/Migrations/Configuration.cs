﻿using SmartJobs.Entities;
using System.Data.Entity.Migrations;

namespace SmartJobs.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<SmartJobsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(SmartJobsContext context)
        {
            // create roles
            context.RoleSet.AddOrUpdate(r => r.Name, GenerateRoles());
        }

        private Roles[] GenerateRoles()
        {
            Roles[] _roles =
            {
                new Roles
                {
                    Name = "Admin"
                },
                new Roles
                {
                    Name = "Employer"
                },
                 new Roles
                {
                    Name = "Candidate"
                }
            };

            return _roles;
        }
    }
}