﻿using System.Configuration;

namespace SmartJobs.Data.Repositories
{
    public abstract class BaseRepository
    {
        protected string ConStr = null;
        public BaseRepository()
        {
            ConStr = ConfigurationManager.ConnectionStrings["SmartJobs"].ConnectionString;
        }
    }
}
