﻿using SmartJobs.Entities;
using SmartJobs.Entities.ViewModel;
using System.Linq;

namespace SmartJobs.Data.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public int Add(Users user)
        {
            using (SmartJobsContext context = new SmartJobsContext())
            {
                // Sav User
                context.UserSet.Add(user);
                context.SaveChanges();

                // Assign Roles
                foreach (var role in user.UserRoles.ToList())
                {
                    context.UserRoleSet.Add(new UserRole
                    {
                        UserId = user.ID,
                        RoleId = role.RoleId
                    });
                }
                context.SaveChanges();

                return user.ID;
            }
        }

        public LoggedInUserModel Authenticate(string username, string password)
        {
            using (SmartJobsContext context = new SmartJobsContext())
            {
                LoggedInUserModel loggedInUserModel = new LoggedInUserModel();
                loggedInUserModel = (from a in context.UserSet
                              join p in context.UserRoleSet
                              on a.ID equals p.UserId
                              select new LoggedInUserModel {
                                  Username = a.Username,
                                  UserKey=a.HashedPassword,
                                  Email = a.Email,
                                  RoleID = p.ID
                              }).FirstOrDefault();

                return loggedInUserModel;
            }
        }

        public void LogOut(string username)
        {
            throw new System.NotImplementedException();
        }
    }
}
