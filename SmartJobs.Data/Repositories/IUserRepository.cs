﻿using SmartJobs.Entities;
using SmartJobs.Entities.ViewModel;

namespace SmartJobs.Data.Repositories
{
    public interface IUserRepository
    {
        int Add(Users user);
        LoggedInUserModel Authenticate(string username, string password);
        void LogOut(string username);
    }
}
