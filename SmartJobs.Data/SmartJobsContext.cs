﻿using SmartJobs.Data.Configurations;
using SmartJobs.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace SmartJobs.Data
{
    public class SmartJobsContext: DbContext
    {
        public SmartJobsContext()
           : base("SmartJobs")
        {
            Database.SetInitializer<SmartJobsContext>(null);
        }

        public virtual void Commit()
        {
            SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new UsersConfiguration());
            modelBuilder.Configurations.Add(new UserRoleConfiguration());
            modelBuilder.Configurations.Add(new RolesConfiguration());
            modelBuilder.Configurations.Add(new UserDeviceConfiguration());
            modelBuilder.Configurations.Add(new JobSeekerConfiguration());
            modelBuilder.Configurations.Add(new EducationConfiuration());
            modelBuilder.Configurations.Add(new SkillConfriguration());
            modelBuilder.Configurations.Add(new JobHistoryConfriguration());
        }

        #region Entity Sets

        public IDbSet<Users> UserSet { get; set; }
        public IDbSet<Roles> RoleSet { get; set; }
        public IDbSet<UserRole> UserRoleSet { get; set; }
        public IDbSet<UserDevice> UserDeviceSet { get; set; }
        public IDbSet<JobSeeker> JobSeekerSet { get; set; }
        public IDbSet<Education> EducationSet { get; set; }
        public IDbSet<Skill> SkillSet { get; set; }
        public IDbSet<JobHistory> JobHistorySet { get; set; }
        #endregion
    }
}
