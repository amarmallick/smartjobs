﻿using SmartJobs.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartJobs.Data.Configurations
{
    public class SkillConfriguration : EntityBaseConfiguration<Skill>
    {
        public SkillConfriguration()
        {
            Property(ur => ur.UserId).IsRequired();
            Property(ur => ur.SkillName).HasMaxLength(100).IsRequired();
            Property(ur => ur.NoOfYearUsed).HasMaxLength(10).IsRequired();
            Property(ur => ur.ProficiencyLevel).HasMaxLength(20).IsRequired();
            Property(ur => ur.LastVersionUsed).HasMaxLength(5).IsRequired();
        }

    }
}
