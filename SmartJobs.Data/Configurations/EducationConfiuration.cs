﻿using SmartJobs.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartJobs.Data.Configurations
{
    public class EducationConfiuration : EntityBaseConfiguration<Education>
    {
        public  EducationConfiuration()
        {
            Property(ur => ur.UserId).IsRequired();
            Property(ur => ur.Qualification).HasMaxLength(30).IsRequired();
            Property(ur => ur.FromDate).IsRequired();
            Property(ur => ur.ToDate);
        }
    }
    
    
    
    
}
