﻿using SmartJobs.Entities;

namespace SmartJobs.Data.Configurations
{
    public class RolesConfiguration : EntityBaseConfiguration<Roles>
    {
        public RolesConfiguration()
        {
            Property(ur => ur.Name).IsRequired().HasMaxLength(50);
        }
    }
}
