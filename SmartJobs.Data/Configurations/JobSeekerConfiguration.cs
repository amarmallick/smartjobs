﻿using SmartJobs.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartJobs.Data.Configurations
{
   public class JobSeekerConfiguration : EntityBaseConfiguration<JobSeeker>
    {
        public JobSeekerConfiguration()
        {
            Property(ur => ur.FirstName).IsRequired().HasMaxLength(50);
            Property(ur => ur.LastName).IsRequired().HasMaxLength(50);
            Property(ur => ur.DOB).IsRequired();
            Property(ur => ur.UserId).IsRequired();
            Property(ur => ur.Mobile).IsRequired().HasMaxLength(15);
            Property(ur => ur.ProfileHeadline).IsRequired().HasMaxLength(100);
            Property(ur => ur.Address).HasMaxLength(200);
            Property(ur => ur.City).HasMaxLength(100);
            Property(ur => ur.AlternamteNo).HasMaxLength(15);
            Property(ur => ur.PostalCode).HasMaxLength(15);
            Property(ur => ur.Countary).HasMaxLength(100);
            Property(ur => ur.AlternamteNo).HasMaxLength(15);
            Property(ur => ur.State).HasMaxLength(100);
            Property(ur => ur.Email).IsRequired().HasMaxLength(150);

            Property(ur => ur.Gender).HasMaxLength(10);
            
            Property(ur => ur.Email).IsRequired().HasMaxLength(150);
        }
    }
}
