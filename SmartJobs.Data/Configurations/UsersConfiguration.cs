﻿using SmartJobs.Entities;

namespace SmartJobs.Data.Configurations
{
    public class UsersConfiguration:EntityBaseConfiguration<Users>
    {
        public UsersConfiguration()
        {
            Property(u => u.Username).IsRequired().HasMaxLength(50);
            Property(u => u.HashedPassword).IsRequired().HasMaxLength(50);
            Property(u => u.Salt).IsRequired().HasMaxLength(200);
            Property(u => u.IsLocked).IsRequired();
            Property(u => u.DateCreated);
            Property(u => u.UserType).IsRequired();
        }
    }
}
