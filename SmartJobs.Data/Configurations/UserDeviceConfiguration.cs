﻿using SmartJobs.Entities;

namespace SmartJobs.Data.Configurations
{
    public class UserDeviceConfiguration: EntityBaseConfiguration<UserDevice>
    {
        public UserDeviceConfiguration()
        {
            Property(c => c.AppVersionOnDevice).IsRequired().HasMaxLength(100);
        }
    }
}
