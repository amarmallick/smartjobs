﻿using System.Web.Mvc;

namespace SmartJobs.API.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("swagger");
        }
    }
}
