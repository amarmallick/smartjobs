﻿using SmartJobs.API.Helpers;
using SmartJobs.Entities;
using SmartJobs.Entities.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartJobs.API.Controllers
{
    /// <summary>
    /// Manage User Account
    /// </summary>
    [RoutePrefix("api/account")]
    public class AccountController : ApiControllerBase
    {
        #region Ctor
        public AccountController()
        {
        }
        #endregion

        [HttpPost]
        [ActionName("register")]
        public IHttpActionResult Register(HttpRequestMessage request, RegisterModel registerUser)
        {
            try
            {
                var passwordSalt = encryptionService.CreateSalt();

                var user = new Users
                {
                    Username = registerUser.Username,
                    Email = registerUser.Email,
                    UserType = registerUser.RoleID, //(int)URole.Candidate,
                    HashedPassword = encryptionService.EncryptPassword(registerUser.Password, passwordSalt),
                    Salt = passwordSalt,
                    DateCreated = DateTime.UtcNow
                };
                int result = userRepository.Add(user);
                if (result > 0)
                {
                    string subject = "Registrated Successfully.";
                    string Name = user.Username;
                    string Pass = "--has been sent on mobile--";
                    string body = "Thanks for registering with Smart Jobs.";

                    // Send Registration Email
                    SendRegisterMail(user.Email, subject, body, Name, Pass);
                    return Ok("success");
                }
                else
                    return BadRequest();
            }
            catch (Exception ex)
            {
                // Log Error
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [ActionName("login")]
        public IHttpActionResult Login(HttpRequestMessage request, string username, string password)
        {
            try
            {
                LoggedInUserModel loggedInUserModel = userRepository.Authenticate(username, password);
                if (loggedInUserModel == null)
                    return BadRequest();

                if (loggedInUserModel.UserKey.Length > 0)
                    return Ok(loggedInUserModel);
                else
                    return BadRequest();
            }
            catch (Exception ex)
            {
                // Log Error
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [ActionName("LogOut")]
        public IHttpActionResult LogOut(HttpRequestMessage request, string username)
        {
            try
            {
                userRepository.LogOut(username);
                return Ok();
            }
            catch (Exception ex)
            {
                // Log Error
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return InternalServerError(ex);
            }
        }

        #region Non Action
        [NonAction]
        void SendRegisterMail(string email, string subject, string body, string Name, string pass)
        {
            try
            {
                using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Template/RegisterUser.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Email}", email);
                body = body.Replace("{Pass}", pass);
                body = body.Replace("{UserName}", Name);

                EmailModel emailModel = new EmailModel
                {
                    Email = email,
                    Subject = subject,
                    EmailBody = body
                };
                emailService.SendEmail(emailModel);
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
    }
}
