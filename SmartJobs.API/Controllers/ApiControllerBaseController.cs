﻿using SmartJobs.API.Helpers;
using SmartJobs.Data.Repositories;
using SmartJobs.Plugins;
using System.Web.Http;

namespace SmartJobs.API.Controllers
{
    public class ApiControllerBase : ApiController
    {
        protected IUserRepository userRepository = null;
        protected IEncryptionService encryptionService = null;
        protected IEmailService emailService = null;

        public ApiControllerBase()
        {
            userRepository = new UserRepository();
            encryptionService = new EncryptionService();
            emailService = new EmailService();
        }
    }
}
