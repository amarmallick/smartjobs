﻿namespace SmartJobs.API.Helpers
{
    public enum URole
    {
        Admin = 1,
        Employer = 2,
        Candidate = 3
    }
}