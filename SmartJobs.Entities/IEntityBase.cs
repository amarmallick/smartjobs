﻿namespace SmartJobs.Entities
{
    public interface IEntityBase
    {
        int ID { get; set; }
    }
}
