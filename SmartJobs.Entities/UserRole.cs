﻿namespace SmartJobs.Entities
{
    public class UserRole:IEntityBase
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public virtual Roles Role { get; set; }
    }
}
