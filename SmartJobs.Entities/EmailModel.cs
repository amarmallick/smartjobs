﻿namespace SmartJobs.Entities
{
    public class EmailModel
    {
        public string Email { get; set; }
        public string Subject { get; set; }
        public string EmailBody { get; set; }
    }
}
