﻿namespace SmartJobs.Entities
{
    public class Roles : IEntityBase
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsSysAdmin { get; set; }
    }
}
