﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartJobs.Entities
{
     public class JobHistory : IEntityBase
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public DateTime FromDate { get; set; }
         public DateTime ToDate { get; set; }
         public string IsCurrentJob { get; set; }
         public decimal Salary { get; set; }
    }
}
