﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartJobs.Entities
{
    public class Education : IEntityBase
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public string Qualification { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
