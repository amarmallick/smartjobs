﻿namespace SmartJobs.Entities.ViewModel
{
    public class LoggedInUserModel
    {
        public string UserKey { get; set; }
        public string Username { get; set; }
        public int RoleID { get; set; }
        public string Email { get; set; }
    }
}
