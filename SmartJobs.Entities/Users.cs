﻿using System;
using System.Collections.Generic;

namespace SmartJobs.Entities
{
    public class Users: IEntityBase
    {
        public int ID { get; set; }
        public Users()
        {
            UserRoles = new List<UserRole>();
        }

        public string Username { get; set; }
        public string Email { get; set; }
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
        public bool IsLocked { get; set; }
        public DateTime DateCreated { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }
        public int UserType { get; set; }
    }
}
