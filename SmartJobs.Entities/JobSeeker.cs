﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartJobs.Entities
{
   public class JobSeeker : IEntityBase
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int UserId { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        
        public string City { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Countary { get; set; }
        public string PostalCode { get; set; }
        public string Mobile { get; set; }
        public string AlternamteNo { get; set; }
        public string ProfileHeadline { get; set; }
        public DateTime LastUpdated { get; set; }
    }
}
