﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartJobs.Entities
{
   public class Skill : IEntityBase
    {
        public int ID { get; set; }
        public string SkillName { get; set; }
        public string NoOfYearUsed { get; set; }
        public string LastVersionUsed { get; set; }
        public int UserId { get; set; }
        public string ProficiencyLevel { get; set; }
    }
}
