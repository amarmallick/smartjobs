﻿using System;

namespace SmartJobs.Entities
{
    public class UserDevice:IEntityBase
    {
        public int ID { get; set; }
        public short DeviceID { get; set; }
        public int UserID { get; set; }
        public short  DeviceTypeId { get; set; }
        public string DeviceOsVersion { get; set; }
        public string SmartPhoneID { get; set; }
        public string AppVersionOnDevice { get; set; }
        public string PushNotificationID { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public string TimeZone { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime NotifyTime { get; set; }
    }
}
