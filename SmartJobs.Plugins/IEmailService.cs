﻿using SmartJobs.Entities;

namespace SmartJobs.Plugins
{
    public interface IEmailService
    {
        void SendEmail(EmailModel emailModel);
    }
}
